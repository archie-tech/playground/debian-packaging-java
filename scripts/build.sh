#!/bin/bash

export DEBEMAIL="hilel14@gmail.com"
export UPSTREAM_VERSION="1.0"

mvn clean install dependency:copy-dependencies

mkdir --parents local
rm -rf local/*
cd local

mkdir hithere-${UPSTREAM_VERSION}
cp ../target/hithere.jar hithere-${UPSTREAM_VERSION}/
cp -R ../target/dependency hithere-${UPSTREAM_VERSION}/
cp ../scripts/hithere.sh hithere-${UPSTREAM_VERSION}/
cp ../resources/Makefile hithere-${UPSTREAM_VERSION}/
tar -czf hithere_1.0.orig.tar.gz hithere-${UPSTREAM_VERSION}

cd hithere-${UPSTREAM_VERSION}
mkdir --parents debian/source
dch --create -v ${UPSTREAM_VERSION}-1 --package hithere "Initial release. (Closes: #XXXXXX)"
cp ../../resources/deb/copyright debian/
cp ../../resources/deb/control debian/
cp ../../resources/deb/rules debian/
cp ../../resources/deb/hithere.dirs debian/
cp ../../resources/deb/source/format debian/source/

 debuild -us -uc
