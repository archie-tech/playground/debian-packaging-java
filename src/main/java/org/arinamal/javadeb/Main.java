package org.arinamal.javadeb;

import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

/**
 * Main
 */
public class Main {

    private static final Logger logger = LoggerFactory.getLogger(Main.class);

    public static void main(String[] args) {
        System.out.println("hello from debian java packaging demo");
        logger.info("Example log from {}", Main.class.getSimpleName());
    }

}
