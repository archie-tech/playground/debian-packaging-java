# Debian Packaging Java

This project demonstrate how to package a java application into a Debian package.
It was tested on Ubuntu 22.04 and might need some adjustments to run on other versions of Ubuntu, Debian or Debian derivatives.

## Quick start

- Run the setup script (only once) to prepare the environment.
- Run the build script.
- Run the test script (this will open a terminal inside a docker container).
   - Install the deb package (example: `apt install /var/opt/hithere_1.0-1_amd64.deb`).
   - Run `hithere.sh` and check the output (some standard greeting).

## Links

* https://wiki.debian.org/Packaging/Intro
* https://wiki.debian.org/Java/Packaging
